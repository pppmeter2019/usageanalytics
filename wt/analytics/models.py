from django.db import models

from wt.att_subscriptions.models import ATTSubscription
from wt.sprint_subscriptions.models import SprintSubscription

from wt.usage.models import DataUsageRecord
from wt.usage.models import VoiceUsageRecord


class UsageByDate(models.Model):
    U_DATA = 'DATA'
    U_VOICE = 'VOICE'

    USAGE_CHOICES = (
        (U_DATA, 'Data'),
        (U_VOICE, 'Voice'),
    )

    id = models.AutoField(primary_key=True)
    usage_date = models.DateField(blank=False, null=False)

    subscription_type = models.CharField(max_length=30, blank=False, null=False)
    subscription_id = models.IntegerField(null=False)

    usage_type = models.CharField(max_length=6, choices=USAGE_CHOICES)
    total_by_date = models.DecimalField(decimal_places=2, max_digits=10, default=0)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['usage_date', 'subscription_type', 'subscription_id', 'usage_type'],
                                    name="UniqueAggregatedRecord")
        ]
