from django.apps import AppConfig


class AnalyticsConfig(AppConfig):
    name = 'wt.analytics'
    label = 'analytics'
