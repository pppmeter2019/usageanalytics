from rest_framework import mixins, viewsets
from wt.analytics.models import UsageByDate
from wt.analytics.serializers import AggregatedUsageByDateSerializer, PriceByDateRangeSerializer
from rest_framework.exceptions import ValidationError
from django.db.models import Value, F, Sum, FloatField

from dateutil.parser import parse, ParserError


class ListOfExceeded(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = UsageByDate.objects.all()
    serializer_class = AggregatedUsageByDateSerializer

    def get_queryset(self):
        max_price = self.request.GET.get('price')
        if not max_price:
            raise ValidationError({'price': 'Price is a mandatory request parameter.'})

        queryset = UsageByDate.objects.all().values('subscription_id', 'usage_type', 'subscription_type').annotate(
            total=Sum('total_by_date')).filter(total__gt=max_price).annotate(overhead=F('total') - max_price)
        return queryset


class PriceByDateRange(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = UsageByDate.objects.all()
    serializer_class = PriceByDateRangeSerializer

    def get_queryset(self):
        from_date = self.request.GET.get('from_date')
        to_date = self.request.GET.get('to_date')
        entity_type = self.request.GET.get('type')
        if not from_date or not to_date or not entity_type:
            raise ValidationError({'detail': 'from_date, to_date and type are mandatory request parameters.'})

        try:
            from_date = parse(from_date).date()
            to_date = parse(to_date).date()
        except ParserError:
            raise ValidationError({'detail': 'Wrong date format'})

        entity_type = entity_type.upper()
        if entity_type not in ('VOICE', 'DATA'):
            raise ValidationError({'type': 'Type must be "VOICE" or "DATA"'})

        all_total = float(
            UsageByDate.objects.filter(usage_date__range=[from_date, to_date]).filter(
                usage_type=entity_type).aggregate(
                all_total=Sum('total_by_date'))['all_total']
        )

        queryset = UsageByDate.objects.filter(
            usage_date__range=[from_date, to_date]
        ).filter(
            usage_type=entity_type
        ).annotate(
            all_subscriptions_total=Value(all_total, FloatField())
        ).values('subscription_id', 'subscription_type', 'all_subscriptions_total').annotate(total=Sum('total_by_date'))
        return queryset
