from rest_framework import serializers


class AggregatedUsageByDateSerializer(serializers.Serializer):
    subscription_id = serializers.IntegerField()
    subscription_type = serializers.CharField()
    usage_type = serializers.CharField()
    total = serializers.DecimalField(max_digits=7, decimal_places=2)
    overhead = serializers.DecimalField(max_digits=7, decimal_places=2)


class PriceByDateRangeSerializer(serializers.Serializer):
    subscription_id = serializers.IntegerField()
    subscription_type = serializers.CharField()
    total = serializers.DecimalField(max_digits=7, decimal_places=2)
    all_subscriptions_total = serializers.DecimalField(max_digits=7, decimal_places=2)
