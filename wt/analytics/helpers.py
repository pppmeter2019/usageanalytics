from django.db.models import Case, Value, F, When, CharField, Sum
from wt.analytics.models import UsageByDate
from django.contrib.contenttypes.models import ContentType
from django.db.models.functions import TruncDate


def get_respective_queryset(current_model, u_type):
    queryset = current_model.objects.annotate(
        day_date=TruncDate('usage_date')
    ).annotate(
        subscription_type=Case(
            When(att_subscription_id__isnull=False, then=Value('ATTSubscription')),
            default=Value('SprintSubscription'),
            output_field=CharField()
        )).annotate(
        subscription_id=Case(
            When(att_subscription_id__isnull=False, then=F('att_subscription_id__id')),
            default=F('sprint_subscription_id__id'),
        )).values(
        'subscription_id', 'day_date',
        'subscription_type',
        usage_type=Value(u_type, CharField())
    ).annotate(total_by_date=Sum('price'))
    return queryset


def fetch_analytics_data():
    for d_model in ('DataUsageRecord', 'VoiceUsageRecord'):
        ct = ContentType.objects.get(model=d_model.lower())
        current_model = ct.model_class()

        u_type = UsageByDate.U_DATA

        if 'voice' in d_model.lower():
            u_type = UsageByDate.U_VOICE

        queryset = get_respective_queryset(current_model, u_type)

        for aggregation in queryset:
            obj, created = UsageByDate.objects.update_or_create(
                usage_date=aggregation['day_date'],
                subscription_type=aggregation['subscription_type'],
                subscription_id=aggregation['subscription_id'],
                usage_type=aggregation['usage_type'],
                defaults={
                    'total_by_date': aggregation['total_by_date'],
                }
            )
