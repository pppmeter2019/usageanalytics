from rest_framework.test import APITestCase
from wt.tests import factories


class BaseAPITestCaseSetup(APITestCase):
    def setUp(self):
        self.first_user = factories.UserFactory()
        self.second_user = factories.UserFactory()
        self.forth_user = factories.UserFactory()
        self.third_user = factories.UserFactory()

        self.plan_1 = factories.PlanFactory()
        self.plan_2 = factories.PlanFactory()
        self.plan_3 = factories.PlanFactory()
        self.plan_4 = factories.PlanFactory()
