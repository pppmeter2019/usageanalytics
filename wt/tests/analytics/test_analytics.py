from wt.tests.base import BaseAPITestCaseSetup
from wt.tests import factories

from django.db.models import Sum

from wt.analytics.models import UsageByDate
from decimal import Decimal
from wt.analytics.helpers import fetch_analytics_data
from rest_framework.reverse import reverse
from datetime import datetime
import pytz
from rest_framework import status
from dateutil.parser import parse


class TestFirstApiAnalytics(BaseAPITestCaseSetup):
    def setUp(self):
        super().setUp()
        self.att_sub_1 = factories.ATTSubscriptionFactory(user=self.first_user, plan=self.plan_1)
        self.sprint_sub_1 = factories.SprintSubscriptionFactory(user=self.second_user, plan=self.plan_2)

        self.att_sub_2 = factories.ATTSubscriptionFactory(user=self.second_user, plan=self.plan_3)
        self.sprint_sub_2 = factories.SprintSubscriptionFactory(user=self.forth_user, plan=self.plan_4)

        self.att_sub_3 = factories.ATTSubscriptionFactory(user=self.forth_user, plan=self.plan_3)
        self.sprint_sub_3 = factories.SprintSubscriptionFactory(user=self.third_user, plan=self.plan_4)

        pst = pytz.timezone('America/Los_Angeles')

        # att1 voice total 100
        factories.VoiceUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/19/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            att_subscription_id=self.att_sub_1,
            price=50
        )

        factories.VoiceUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/20/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            att_subscription_id=self.att_sub_1,
            price=40
        )
        factories.VoiceUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/20/18 14:55:26', '%m/%d/%y %H:%M:%S')),
            att_subscription_id=self.att_sub_1,
            price=10
        )
        # att1 data total 120
        factories.DataUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/19/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            att_subscription_id=self.att_sub_1,
            price=50
        )
        factories.DataUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/24/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            att_subscription_id=self.att_sub_1,
            price=70
        )
        # sprint1 voice total 90
        factories.VoiceUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/29/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            sprint_subscription_id=self.sprint_sub_1,
            price=50
        )

        factories.VoiceUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/30/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            sprint_subscription_id=self.sprint_sub_1,
            price=40
        )
        # sprint2 data total 8
        factories.DataUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/19/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            sprint_subscription_id=self.sprint_sub_2,
            price=3
        )
        factories.DataUsageFactory(
            usage_date=pst.localize(datetime.strptime('09/24/18 13:55:26', '%m/%d/%y %H:%M:%S')),
            sprint_subscription_id=self.sprint_sub_2,
            price=5
        )

    def test_list_exceeded_subscriptions_fail_parameter_absence(self):
        fetch_analytics_data()

        price = 10
        params = {
            'a-price': price,
        }

        response = self.client.get(reverse('api:usagebydate-list'), params, format='json')
        self.assertContains(response, 'Price is a mandatory request parameter', status_code=status.HTTP_400_BAD_REQUEST)

    def test_list_exceeded_subscriptions_success(self):

        fetch_analytics_data()

        price = 10
        params = {
            'price': price,
        }

        response = self.client.get(reverse('api:usagebydate-list'), params, format='json')
        self.assertEqual(len(response.json()), 3)

        result = {}
        for value in response.json():
            result[value['total']] = value['overhead']

        for expected_total in ('100.00', '120.00', '90.00'):
            self.assertEqual(Decimal(result[expected_total]), Decimal(expected_total) - price)

    def test_list_by_date_and_type_success(self):
        fetch_analytics_data()
        for usage_type in ('DATA', 'VOICE'):
            params = {
                'from_date': '2018-09-19 23:55',
                'to_date': '2018-09-30',
                'type': usage_type,
            }

            response = self.client.get(reverse('api:price_by_date-list'), params, format='json')

            from_date = parse(params['from_date']).date()
            to_date = parse(params['to_date']).date()

            queryset_filtered_by_date = UsageByDate.objects.filter(usage_date__range=[from_date, to_date])

            list_of_unique_subscriptions = list(queryset_filtered_by_date.filter(usage_type=params['type']).values(
                'subscription_type', 'subscription_id').distinct())

            total_from_db = 0
            by_subscription_total_from_db = {}
            for item in list_of_unique_subscriptions:
                sum_ = (queryset_filtered_by_date.filter(
                    subscription_type=item['subscription_type'],
                    subscription_id=item['subscription_id'],
                    usage_type=params['type']
                ).aggregate(total_by_subscription=Sum('total_by_date')))['total_by_subscription']
                by_subscription_total_from_db[
                    '_'.join([item['subscription_type'], str(item['subscription_id'])])] = sum_
                total_from_db += sum_

            self.assertEqual(len(by_subscription_total_from_db.items()), len(response.json()))

            for item in response.json():
                key = '_'.join([item['subscription_type'], str(item['subscription_id'])])
                self.assertEqual(by_subscription_total_from_db[key], Decimal(item['total']))
                self.assertEqual(total_from_db, Decimal(item['all_subscriptions_total']))

    def test_list_by_date_and_type_fail_wrong_date(self):
        fetch_analytics_data()

        params = {
            'from_date': '2018sada-09-19 23:55',
            'to_date': '2018-09-30',
            'type': 'DATA',
        }

        response = self.client.get(reverse('api:price_by_date-list'), params, format='json')
        self.assertContains(response, 'Wrong date format', status_code=status.HTTP_400_BAD_REQUEST)

    def test_list_by_date_and_type_fail_wrong_type(self):
        fetch_analytics_data()

        params = {
            'from_date': '2018-09-19 23:55',
            'to_date': '2018-09-30',
            'type': 'DATadsdA',
        }

        response = self.client.get(reverse('api:price_by_date-list'), params, format='json')
        self.assertContains(response, 'Type must be ', status_code=status.HTTP_400_BAD_REQUEST)

    def test_list_by_date_and_type_fail_parameters_absence(self):
        fetch_analytics_data()

        params = {
            'from_date': '2018-09-19 23:55',

            'type': 'DATadsdA',
        }

        response = self.client.get(reverse('api:price_by_date-list'), params, format='json')
        self.assertContains(response, 'from_date, to_date and type are mandatory request parameters',
                            status_code=status.HTTP_400_BAD_REQUEST)
