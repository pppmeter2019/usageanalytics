import random
import string
import factory
from django.contrib.auth.models import User
from wt.sprint_subscriptions.models import SprintSubscription
from wt.att_subscriptions.models import ATTSubscription
from wt.usage.models import VoiceUsageRecord, DataUsageRecord
from wt.plans.models import Plan
from datetime import datetime
import pytz


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('username',)

    username = factory.Faker('user_name')
    email = factory.Faker('company_email')

    is_active = True
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')


class PlanFactory(factory.DjangoModelFactory):
    class Meta:
        model = Plan
        django_get_or_create = ('name',)

    name = factory.Faker('text', max_nb_chars=50, ext_word_list=None)
    price = factory.Faker('pydecimal', left_digits=3, right_digits=2)
    data_available = factory.Faker('pyint', max_value=999999999999999999)


class ATTSubscriptionFactory(factory.DjangoModelFactory):
    class Meta:
        model = ATTSubscription
        django_get_or_create = ('user',)

    device_id = factory.LazyAttribute(lambda x: ''.join(random.choice(string.ascii_letters) for _ in range(20)))
    phone_number = factory.Faker('phone_number')
    phone_model = factory.Faker('text', max_nb_chars=128)
    network_type = factory.Faker('text', max_nb_chars=5)


class SprintSubscriptionFactory(factory.DjangoModelFactory):
    class Meta:
        model = SprintSubscription
        django_get_or_create = ('user',)

    device_id = factory.LazyAttribute(lambda x: ''.join(random.choice(string.ascii_letters) for _ in range(20)))
    phone_number = factory.Faker('phone_number')
    phone_model = factory.Faker('text', max_nb_chars=128)

    sprint_id = factory.LazyAttribute(lambda x: ''.join(random.choice(string.ascii_letters) for _ in range(5)))



class DataUsageFactory(factory.DjangoModelFactory):
    class Meta:
        model = DataUsageRecord


    usage_date = factory.Faker(
        'date_time_between_dates',
        datetime_start=datetime.strptime('09/19/18 13:55:26', '%m/%d/%y %H:%M:%S'),
        datetime_end=datetime.strptime('11/19/18 13:55:26', '%m/%d/%y %H:%M:%S'),
        tzinfo=pytz.timezone('America/New_York')
    )

    kilobytes_used = factory.Faker('pyint', min_value=0, max_value=99999)


class VoiceUsageFactory(factory.DjangoModelFactory):
    class Meta:
        model = VoiceUsageRecord

    usage_date = factory.Faker(
        'date_time_between_dates',
        datetime_start=datetime.strptime('09/19/18 13:55:26', '%m/%d/%y %H:%M:%S'),
        datetime_end=datetime.strptime('11/19/18 13:55:26', '%m/%d/%y %H:%M:%S'),
        tzinfo=pytz.timezone('America/New_York')
    )
    seconds_used = factory.Faker('pyint', min_value=1, max_value=9999)
